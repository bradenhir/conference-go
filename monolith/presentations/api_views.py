from django.http import JsonResponse
from common.json import ModelEncoder

from .models import Presentation


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


def api_list_presentations(request, conference_id):

    presentations = [
        {
            "title": p.title,
            "status": p.status.name,
            "href": p.get_api_url(),
        }
        for p in Presentation.objects.filter(conference=conference_id)
    ]
    return JsonResponse({"presentations": presentations})


def api_show_presentation(request, id):

    presentation = Presentation.objects.get(id=id)
    return JsonResponse(
        #        {
        #           "presenter_name": presentation.presenter_name,
        #            "company_name": presentation.company_name,
        #            "title": presentation.title,
        #            "synopsis": presentation.synopsis,
        #            "created": presentation.created,
        #            "status": presentation.status.name,
        #            "conference": {
        #                "name": presentation.id,
        #                "href": presentation.get_api_url(),
        #            },
        #        }
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
